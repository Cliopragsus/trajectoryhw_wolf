﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelTurner : MonoBehaviour
{
    private Transform orbitCenter;
    public float distanceFromCenter;
    private float rotateValue;
    public float rotationSpeed = 1;
    public float shootingPower = 1;
    public float gravityFactor = 9.81f;
    private bool beenShot = false;
    float timePassed = 0;
    private Vector3 startPos;

    private void Start()
    {
        orbitCenter = GameObject.Find("CenterObject").transform;
        startPos = gameObject.transform.position;
        orbitCenter.gameObject.GetComponent<LineRenderer>().SetPosition(1, new Vector3(-3, shootingPower * 0.3f, 0));
    }

    private void Update()
    {
        if (Input.GetButton("Jump"))
        {
            shootingPower += 3 * Time.deltaTime;
            orbitCenter.gameObject.GetComponent<LineRenderer>().SetPosition(1, new Vector3(-3, shootingPower * 0.3f, 0));
        }

        if (Input.GetButtonUp("Jump"))
        {
            orbitCenter.gameObject.GetComponent<MeshRenderer>().enabled = false;
            timePassed = 0;
            beenShot = true;
        }

        if (Input.GetKeyDown("q"))
        {
            beenShot = false;
            gameObject.transform.position = startPos;
            shootingPower = 5;
            orbitCenter.gameObject.GetComponent<MeshRenderer>().enabled = true;
            orbitCenter.gameObject.GetComponent<LineRenderer>().SetPosition(1, new Vector3(-3, shootingPower * 0.3f, 0));
        }

        if (beenShot)
        {
            timePassed += Time.deltaTime;
            Vector3 trajectory = orbitCenter.transform.position;
            trajectory.x += shootingPower * timePassed * Mathf.Cos(rotateValue);
            trajectory.y += shootingPower * timePassed * Mathf.Sin(rotateValue) - 0.5f * gravityFactor * timePassed * timePassed;
            gameObject.transform.position = trajectory;
        }

        if (!beenShot)
        {
            if (Input.GetKey("a"))
            {
                if (rotateValue > -1)
                {
                    rotateValue -= 0.1f * rotationSpeed;
                }
            }
            if (Input.GetKey("d") && rotateValue < 1)
            {
                rotateValue += 0.1f * rotationSpeed;
            }

            Vector3 myPosition = orbitCenter.transform.position;
            myPosition.x += distanceFromCenter * Mathf.Sin(rotateValue);
            myPosition.y += distanceFromCenter * Mathf.Cos(rotateValue);

            transform.position = myPosition;
        }

    }
}
